const firebase = require('firebase');

function getUserData(uid) {
    const db = firebase.firestore();
    let docRef = db.collection("users").doc(uid);

    return docRef.get();
}

export default {
    install(Vue, opt) {
        if ( !opt.projectSlug )
            throw 'Missing project slug';

        firebase.initializeApp(opt.config);

        Vue.mixin({
            data() {
                return {
                    isAuthenticated: false
                }
            },
            beforeMount() {
                firebase.auth().onAuthStateChanged(user => {
                    if ( user ) {

                        getUserData(user.uid).then(doc => {
                            if (doc.exists) {
                                this.isAuthenticated = doc.data().projects.includes(opt.projectSlug) || doc.data().projects.includes('all');
                            } else {
                                this.isAuthenticated = false;
                            }
                        }).catch(() => {
                            this.isAuthenticated = false;
                        })

                    } else {
                        this.isAuthenticated = false;
                    }
                    return false;
                    // this.isAuthenticated = !!user;
                })
            },
            methods: {
                logIn(email, password) {
                    this.$firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION).then(() => {
                        this.$firebase.auth().signInWithEmailAndPassword(email, password);
                            // .then(() => {
                            //     commit(SET_USER, Firebase.auth().currentUser);
                            // }).catch(err => {
                            //     commit(AUTH_ERROR, err);
                            //     localStorage.removeItem('user-token'); // if the request fails, remove any possible user token if possible
                            // })
                    });
                },
                signOut() {
                    this.$firebase.auth().signOut();
                }
            },
            computed: {
                $firebase() {
                    return firebase;
                },
            }
        });
    }
}